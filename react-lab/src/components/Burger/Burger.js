import React from 'react';

import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

// Wrapper for burger ingredients
const burger = (props) => {
    // Create an array of key value pares of a JS object
    // Object.keys() gives an array of keys
    let transformedIngredients = Object.keys(props.ingredients)
        // we can now loop through transformedIngredients with the map() method
        .map(igKey => {
            // transform string value to an array with as many elements as in the props array, this allows for 2 x cheese etc.
            // Array() returns an Array, you can use Array(3) to return an array with 3 undef values (JS code)
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            });
        })
        // Built in array function that transofrms an array into something else (arr, el)
        .reduce((arr, el) => {
            return arr.concat(el)
        }, []);
        if (transformedIngredients.length === 0) {
            transformedIngredients = <p>Please start adding ingredients!</p>
        }
        console.log(transformedIngredients)
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger;