import React, { Component } from 'react';

import classes from './Modal.css';
import Aux from '../../../hoc/Ax/Ax';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {
    
    // Make sure the component is unneccerasily rendered
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show;
    }

    componentWillUpdate() {
        console.log('[Modal] WillUpdate');
    }

    render() {
        return(
            <Aux>
                <Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
                <div
                    className={classes.Modal}
                    style={{
                        // vh = viewpoint height, -100 will put it outside of the visible area
                        transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: this.props.show ? '1' : '1'
                    }}>
                    {this.props.children}
                </div>
            </Aux>

        );
    }
}

export default Modal;