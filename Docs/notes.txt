Components =  Stateless components, created as functions

Containers = Stateful components created with class keywors

Assets = Stores pictures/etc.

Userful snippets = 
    imr->   import React from 'react'
    enf->   export const <functionName> = (<params>) => { }

// Validation for props
import PropTypes from 'prop-types';

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
}